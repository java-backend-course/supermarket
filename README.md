Interaction with the database occurs using JDBC.

A certain number of products is randomly generated (the number of products can be changed in the properties file).

Before inserting into the database, DTOs are checked for validity using hibernate-validator.

The result of the program displays the address of the store with the largest number of products of a certain type, and the type of product is requested from the console, a command line parameter.