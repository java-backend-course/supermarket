drop schema if exists market cascade;

create schema market;

set search_path to market;

create table market.type_of_product (
    id serial primary key,
    name varchar(50) not null
);

create table market.store_addresses (
    id serial primary key,
    name varchar(200) not null
);

create table market.products (
    id bigserial primary key,
    type_id integer not null references market.type_of_product(id)
    on delete cascade on update cascade,
    name varchar(50) not null,
    quantity integer default 0,
    address_id integer not null references market.store_addresses(id)
    on delete cascade on update cascade
);