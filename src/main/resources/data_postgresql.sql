insert into market.type_of_product(name) values
('stationery'),
('plumbing'),
('construction'),
('furniture'),
('lighting'),
('clothing')
;

insert into market.store_addresses(name) values
('9558 Miller Rd. Michigan City, IN 46360'),
('8568 Park Dr. Marlborough, MA 01752'),
('692 Eagle Lane Gettysburg, PA 17325'),
('100 Greystone Dr. Fairburn, GA 30213'),
('27 Prospect Rd. High Point, NC 27265'),
('25 Cemetery Court North Royalton, OH 44133'),
('87 Elm Court Marion, NC 28752'),
('5 Acacia Court Lake Villa, IL 60046'),
('602 Aspen St. Braintree, MA 02184'),
('95 Wild Rose Ave. Rochester, NY 14606')
;