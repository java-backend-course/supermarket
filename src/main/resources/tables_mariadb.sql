use pryvarnikov;

drop table if exists pryvarnikov.products;
drop table if exists pryvarnikov.type_of_product;
drop table if exists pryvarnikov.store_addresses;

create table pryvarnikov.type_of_product (
    id integer auto_increment primary key,
    name varchar(50) not null
);

create table pryvarnikov.store_addresses (
    id integer auto_increment primary key,
    name varchar(200) not null
);

create table pryvarnikov.products (
    id bigint auto_increment primary key,
    type_id integer not null,
    name varchar(50) not null,
    quantity integer default 0,
    address_id integer not null,
    constraint fk_type
    foreign key(type_id) references pryvarnikov.type_of_product(id)
    on delete cascade on update cascade,
    constraint fk_address
    foreign key(address_id) references pryvarnikov.store_addresses(id)
    on delete cascade on update cascade
);

