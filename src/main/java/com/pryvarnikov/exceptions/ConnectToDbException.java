package com.pryvarnikov.exceptions;

public class ConnectToDbException extends Exception {
    public ConnectToDbException() {
    }

    public ConnectToDbException(String message) {
        super(message);
    }
}
