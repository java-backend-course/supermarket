package com.pryvarnikov.exceptions;

public class AddProductsException extends Exception {
    public AddProductsException() {
    }

    public AddProductsException(String message) {
        super(message);
    }
}
