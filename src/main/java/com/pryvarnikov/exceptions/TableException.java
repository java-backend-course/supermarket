package com.pryvarnikov.exceptions;

public class TableException extends Exception {
    public TableException() {
    }

    public TableException(String message) {
        super(message);
    }
}
