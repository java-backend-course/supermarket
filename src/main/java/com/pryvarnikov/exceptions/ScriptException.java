package com.pryvarnikov.exceptions;

public class ScriptException extends Exception {
    public ScriptException() {
    }

    public ScriptException(String message) {
        super(message);
    }
}
