package com.pryvarnikov.exceptions;

public class InvalidArgumentsException extends Exception {
    public InvalidArgumentsException() {
    }

    public InvalidArgumentsException(String message) {
        super(message);
    }
}
