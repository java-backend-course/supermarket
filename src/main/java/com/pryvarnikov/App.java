package com.pryvarnikov;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.LocalTime;

public class App {
    private static final Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        LocalTime timeStart = LocalTime.now();
        logger.info("Application started");

        Market market = new Market();
        market.start(args);

        LocalTime timeEnd = LocalTime.now();
        Duration duration = Duration.between(timeStart, timeEnd);
        System.out.printf("Application execution time: %d minutes %d seconds",
                duration.toMinutes(),
                duration.toSecondsPart());
        logger.info("Application execution time: {} minutes {} seconds",
                duration.toMinutes(),
                duration.toSecondsPart());
        logger.info("Application finished\n");
    }
}
