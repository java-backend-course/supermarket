package com.pryvarnikov.dto;

import com.pryvarnikov.validators.RestrictionsOnVowels;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class Product {
    private Long id;
    private Integer typeId;
    @Size(min = 3, max = 50, message = "{sizeMessage}")
    @RestrictionsOnVowels(message = "{restrictionsOnVowelsMessage}", minNumberOfVowels = 3)
    private String name;
    private Integer quantity;
    private Integer addressId;

    public Product() {
    }

    public Product(Integer typeId, String name, Integer quantity, Integer addressId) {
        this.typeId = typeId;
        this.name = name;
        this.quantity = quantity;
        this.addressId = addressId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (!id.equals(product.id)) return false;
        return name.equals(product.name);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", typeId=" + typeId +
                ", name='" + name + '\'' +
                ", quantity=" + quantity +
                ", addressId=" + addressId +
                '}';
    }
}
