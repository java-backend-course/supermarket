package com.pryvarnikov.dto;

public class TypeOfProduct {
    private int id;
    private String name;

    public TypeOfProduct() {
    }

    public TypeOfProduct(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TypeOfProduct that = (TypeOfProduct) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return "TypeOfProduct{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
