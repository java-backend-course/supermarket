package com.pryvarnikov.db;

import com.pryvarnikov.exceptions.ConnectToDbException;
import com.pryvarnikov.exceptions.ScriptException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

public class ScriptReader {
    private static final Logger logger = LoggerFactory.getLogger(ScriptReader.class);

    public void executeScript(String pathToScript) throws ScriptException {
        StringBuilder sqlQuery = new StringBuilder();
        Connection connection;
        try {
            connection = com.pryvarnikov.db.Connection.get();
            logger.info("Connection received");
        } catch (ConnectToDbException e) {
            logger.error("Unable to get database connection");
            logger.info(Arrays.toString(e.getStackTrace()));
            throw new ScriptException();
        }
        try (BufferedReader reader = new BufferedReader(new FileReader(pathToScript))) {
            try (Statement statement = connection.createStatement()) {
                String line;
                while ((line = reader.readLine()) != null) {
                    if (line.isEmpty() || line.isBlank()) {
                        continue;
                    }
                    sqlQuery.append(line);
                    if (line.endsWith(";")) {
                        try {
                            statement.execute(sqlQuery.toString());
                            logger.info("Execute query:\n{}", sqlQuery);
                        } catch (SQLException e) {
                            logger.error("An error occurred while executing the query \n{}", sqlQuery);
                            logger.info(Arrays.toString(e.getStackTrace()));
                            throw new ScriptException();
                        }
                        sqlQuery.delete(0, sqlQuery.length());
                    } else {
                        sqlQuery.append(System.lineSeparator());
                    }
                }
            } catch (SQLException e) {
                logger.error("An error occurred while trying to create a statement");
                logger.info(Arrays.toString(e.getStackTrace()));
                throw new ScriptException();
            }
        } catch (IOException e) {
            logger.error("There was a problem while trying to read from a file {}", pathToScript);
            logger.info(Arrays.toString(e.getStackTrace()));
            throw new ScriptException();
        }

        try {
            connection.commit();
            logger.info("All changes were committed to the database");
        } catch (SQLException e) {
            logger.warn("Failed to commit changes to database");
            throw new ScriptException();
        }
        logger.info("The script {} was executed successfully", pathToScript);
    }

    public String getDatabaseName() throws ScriptException {
        try {
            Connection connection = com.pryvarnikov.db.Connection.get();
            return connection.getMetaData().getDriverName().split(" ")[0].toLowerCase();
        } catch (ConnectToDbException | SQLException e) {
            logger.error("An error occurred while trying to get the database name");
            throw new ScriptException();
        }
    }
}
