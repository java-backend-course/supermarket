package com.pryvarnikov.db;

import com.pryvarnikov.exceptions.ConnectToDbException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Properties;

public class Connection {
    private static java.sql.Connection connection;
    private static final Logger logger = LoggerFactory.getLogger(Connection.class);

    private Connection() {
    }

    public static java.sql.Connection get() throws ConnectToDbException {
        if (connection == null) {
            init();
        }
        return connection;
    }

    private static void init() throws ConnectToDbException {
        final String JDBC_DRIVER, DB_URL, USER, PASS;
        Properties appProps = new Properties();
        try {
            String pathToDbSettings = "src/main/resources/db_settings";
            appProps.load(new InputStreamReader(new FileInputStream(pathToDbSettings), StandardCharsets.UTF_8));
            logger.info("Properties file loaded successfully");
        } catch (IOException e) {
            logger.error("Error reading db configuration file 'db_settings': {}", e.getMessage());
            logger.info(Arrays.toString(e.getStackTrace()));
            throw new ConnectToDbException();
        }

        checkProperties(appProps);

        JDBC_DRIVER = appProps.getProperty("driver");
        DB_URL = appProps.getProperty("url");
        USER = appProps.getProperty("user");
        PASS = appProps.getProperty("password");

        try {
            Class.forName(JDBC_DRIVER);
        } catch (ClassNotFoundException e) {
            logger.error("Driver {} not found", JDBC_DRIVER);
            logger.info(Arrays.toString(e.getStackTrace()));
            throw new ConnectToDbException();
        }

        try {
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (SQLException e) {
            logger.error("Unable to get database connection. URL: {} user: {}", DB_URL, USER);
            logger.info(Arrays.toString(e.getStackTrace()));
            throw new ConnectToDbException();
        }

        try {
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            logger.error("Unable to disable connection auto-commit mode");
            logger.info(Arrays.toString(e.getStackTrace()));
            throw new ConnectToDbException();
        }
        logger.info("Connection created successfully");
    }

    private static void checkProperties(Properties appProps) throws ConnectToDbException {
        StringBuilder missingProperties = new StringBuilder();
        if (!appProps.containsKey("driver")) {
            missingProperties.append("'driver' ");
        }
        if (!appProps.containsKey("url")) {
            missingProperties.append("'url' ");
        }
        if (!appProps.containsKey("user")) {
            missingProperties.append("'user' ");
        }
        if (!appProps.containsKey("password")) {
            missingProperties.append("'password' ");
        }
        if (missingProperties.length() != 0) {
            logger.error("The following values are missing from the file 'db_settings': {}", missingProperties);
            throw new ConnectToDbException();
        }
        logger.info("The database connection properties file fields are valid");
    }
}
