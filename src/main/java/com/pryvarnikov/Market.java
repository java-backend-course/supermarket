package com.pryvarnikov;

import com.pryvarnikov.db.ScriptReader;
import com.pryvarnikov.dto.Product;
import com.pryvarnikov.exceptions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Market {
    private static final Logger logger = LoggerFactory.getLogger(Market.class);
    private int numberOfProducts;
    private final HashMap<String, Integer> TYPE_OF_PRODUCT;

    public Market() {
        TYPE_OF_PRODUCT = new HashMap<>();
    }

    public void start(String[] args) {
        try {
            runScripts();
            validateArgumments(args);
            addProducts();
            Connection connection = com.pryvarnikov.db.Connection.get();
            HashMap<String, Integer> biggestMarket = getBiggestMarket(connection);
            System.out.printf("The largest number of goods of type '%s' is in the store: %s.%n",
                    TYPE_OF_PRODUCT.entrySet().iterator().next().getKey(),
                    biggestMarket.entrySet().iterator().next().getKey());
            System.out.printf("Total number of products: %d.%n",
                    biggestMarket.entrySet().iterator().next().getValue());
            connection.close();
        } catch (ScriptException | InvalidArgumentsException | AddProductsException | ConnectToDbException |
                 SQLException e) {
            logger.info("Something went wrong...");
        }
    }

    private void validateArgumments(String[] args) throws InvalidArgumentsException {
        if (args.length != 2) {
            logger.error("There must be two arguments. The first is the number of products, the second is the type of products");
            ArrayList<String> listOfProductTypes = getListOfProductTypes();
            System.out.println("List of product types:");
            listOfProductTypes.forEach(System.out::println);
            throw new InvalidArgumentsException();
        }
        if (args[1] == null || args[1].isEmpty() || args[1].isBlank()) {
            logger.error("Product type not specified");
            throw new InvalidArgumentsException();
        }
        isColumnExists(args[1]);
        if (TYPE_OF_PRODUCT.get(args[1]) == 0) {
            throw new InvalidArgumentsException();
        }
        try {
            numberOfProducts = Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            logger.error("The number of products is indicated incorrectly");
            throw new InvalidArgumentsException();
        }
        if (numberOfProducts <= 0) {
            logger.error("The number of products must be greater than zero");
            throw new InvalidArgumentsException();
        }
        logger.info("Validation of arguments was successful");
    }

    private void isColumnExists(String typeOfProduct) {
        String query = "select id from type_of_product where name = ?;";
        try {
            Connection connection = com.pryvarnikov.db.Connection.get();
            try (PreparedStatement statement = connection.prepareStatement(query)) {
                statement.setString(1, typeOfProduct);
                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    TYPE_OF_PRODUCT.put(typeOfProduct, resultSet.getInt("id"));
                    logger.info("The product type '{}' with ID {} is found in the table",
                            typeOfProduct,
                            TYPE_OF_PRODUCT.get(typeOfProduct));
                } else {
                    logger.error("The column '{}' is missing", typeOfProduct);
                }
            }
        } catch (ConnectToDbException e) {
            logger.error("Error checking if column exists");
        } catch (SQLException e) {
            logger.error("Statement error while checking for column availability");
        }
    }

    private ArrayList<String> getListOfProductTypes() throws InvalidArgumentsException {
        ArrayList<String> listOfProductTypes = new ArrayList<>();
        try {
            Connection connection = com.pryvarnikov.db.Connection.get();
            try (Statement statement = connection.createStatement()) {
                String query = "select name from type_of_product;";
                ResultSet resultSet = statement.executeQuery(query);
                while (resultSet.next()) {
                    listOfProductTypes.add(resultSet.getString(1));
                }
            } catch (SQLException e) {
                logger.error("Statement error while trying to get product list");
                throw new InvalidArgumentsException();
            }
        } catch (ConnectToDbException e) {
            logger.error("Error retrieving list of product types");
            throw new InvalidArgumentsException();
        }
        logger.info("The list of product types was successfully read from the database");
        return listOfProductTypes;
    }

    private void runScripts() throws ScriptException {
        ScriptReader scriptReader = new ScriptReader();
        String dbName = scriptReader.getDatabaseName();
        if (dbName.equals("mariadb")) {
            logger.info("Starting the execution of the script for the MariaDB database");
            scriptReader.executeScript("src/main/resources/tables_mariadb.sql");
            scriptReader.executeScript("src/main/resources/data_mariadb.sql");
        } else if (dbName.equals("postgresql")) {
            logger.info("Starting the execution of the script for the PostgreSQL database");
            scriptReader.executeScript("src/main/resources/tables_postgresql.sql");
            scriptReader.executeScript("src/main/resources/data_postgresql.sql");
        } else {
            logger.error("Unknown database: {}", dbName);
        }
    }

    private void addProducts() throws AddProductsException {
        Connection connection;
        try {
            connection = com.pryvarnikov.db.Connection.get();
        } catch (ConnectToDbException ex) {
            logger.error("Error inserting products into the table");
            throw new AddProductsException();
        }

        int numberOfPossibleTypes, numberOfStoreAddresses;
        try {
            numberOfPossibleTypes = countRows(connection, "type_of_product");
            numberOfStoreAddresses = countRows(connection, "store_addresses");
        } catch (TableException e) {
            logger.error("Error inserting products into the table: {}", e.getMessage());
            logger.info(Arrays.toString(e.getStackTrace()));
            throw new AddProductsException();
        }

        BlockingQueue<Product> validatedProducts = new ArrayBlockingQueue<>(numberOfProducts);
        ProductFactory productFactory = new ProductFactory(validatedProducts,
                numberOfProducts,
                numberOfPossibleTypes,
                numberOfStoreAddresses);
        productFactory.start();

        int batchSize = 0;
        String insertProductSQL = "insert into products (type_id, name, quantity, address_id) values (?,?, ?, ?);";
        Product product;
        try (PreparedStatement productStatement = connection.prepareStatement(insertProductSQL)) {
            for (int i = 0; i < numberOfProducts; i++) {
                progressBar(i, numberOfProducts - 1);
                product = validatedProducts.take();
                productStatement.setInt(1, product.getTypeId());
                productStatement.setString(2, product.getName());
                productStatement.setInt(3, product.getQuantity());
                productStatement.setInt(4, product.getAddressId());
                productStatement.addBatch();
                batchSize++;
                if (batchSize == 32768) {
                    productStatement.executeBatch();
                    connection.commit();
                    batchSize = 0;
                }
            }
            productStatement.executeBatch();
            connection.commit();
        } catch (SQLException e) {
            logger.error("Unable to insert a product into the table");
            throw new AddProductsException();
        } catch (InterruptedException e) {
            logger.error("Unable to read product from validated queue");
            throw new AddProductsException();
        }

        logger.info("{} products have been successfully added", numberOfProducts);
    }

    private HashMap<String, Integer> getBiggestMarket(Connection connection) {
        String query = "select \n" +
                "sum(quantity) as sum_quantity, sa.name \n" +
                "from products p \n" +
                "join store_addresses sa \n" +
                "on p.address_id = sa.id \n" +
                "where type_id = ? \n" +
                "group by address_id, sa.name \n" +
                "order by sum_quantity desc \n" +
                "limit 1;";
        HashMap<String, Integer> biggestMarket = new HashMap<>();
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, TYPE_OF_PRODUCT.entrySet().iterator().next().getValue());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                biggestMarket.put(resultSet.getString("name"), resultSet.getInt("sum_quantity"));
                logger.info("The address of the store with the largest number of products of type '{}': {}. Quantity of goods: {}",
                        TYPE_OF_PRODUCT.entrySet().iterator().next().getKey(),
                        resultSet.getString("name"),
                        resultSet.getInt("sum_quantity"));
            } else {
                biggestMarket.put("There is no store", 0);
                logger.info("Product type '{}' is not found in any store", TYPE_OF_PRODUCT.entrySet().iterator().next().getKey());
            }
        } catch (SQLException e) {
            logger.error("It is not possible to get the address of the store with the most products");
        }
        return biggestMarket;
    }

    private int countRows(Connection connection, String tableName) throws TableException {
        int rowCount = 0;
        try (Statement countRowsStatement = connection.createStatement()) {
            ResultSet resultSet = countRowsStatement.executeQuery("select count(*) from " + tableName + ";");
            if (resultSet.next()) {
                rowCount = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new TableException(String.format("Failed to read number of rows from table '%s'", tableName));
        }
        if (rowCount == 0) {
            throw new TableException(String.format("The '%s' table is empty", tableName));
        }
        return rowCount;
    }

    public void progressBar(long processed, long total) {
        final int ONE_HUNDRED_PERCENT = 100;
        final int MAX_BAR_SIZE = 60;
        if (processed > total || total == 0) {
            return;
        }
        long donePercent = (ONE_HUNDRED_PERCENT * processed) / total;
        int donePercentBar = (int) donePercent * MAX_BAR_SIZE / ONE_HUNDRED_PERCENT;
        char symbolDefault = '.';
        char symbolDone = '#';
        String bar = new String(new char[MAX_BAR_SIZE]).replace('\0', symbolDefault) + "]";
        String barDone = "[" + String.valueOf(symbolDone).repeat(Math.max(0, donePercentBar));
        System.out.print("\r" + barDone + bar.substring(donePercentBar) + " " + donePercent + "%");
        if (processed == total) {
            System.out.print("\n");
        }
    }
}
