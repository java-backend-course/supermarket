package com.pryvarnikov;

import com.pryvarnikov.dto.Product;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;
import java.util.SplittableRandom;
import java.util.concurrent.BlockingQueue;

public class ProductFactory extends Thread {
    private final Logger logger = LoggerFactory.getLogger(ProductFactory.class);
    private final BlockingQueue<Product> products;
    private final int numberOfPossibleTypes;
    private final int numberOfStoreAddresses;
    private final int numberOfProducts;

    public ProductFactory(BlockingQueue<Product> products,
                          int numberOfProducts,
                          int numberOfPossibleTypes,
                          int numberOfStoreAddresses) {
        this.products = products;
        this.numberOfProducts = numberOfProducts;
        this.numberOfPossibleTypes = numberOfPossibleTypes;
        this.numberOfStoreAddresses = numberOfStoreAddresses;
    }

    @Override
    public void run() {
        Product product;
        int typeId, quantity, addressId;
        String name;
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<Product>> constraintViolations;
        int numberRejectedOfProducts = 0;
        SplittableRandom splittableRandom = new SplittableRandom();

        for (int i = numberOfProducts; i > 0; i--) {
            typeId = splittableRandom.nextInt(1, numberOfPossibleTypes + 1);
            addressId = splittableRandom.nextInt(1, numberOfStoreAddresses + 1);
            quantity = splittableRandom.nextInt(1, 101);
            name = RandomStringUtils.randomAlphabetic(3, 51);
            product = new Product(typeId, name, quantity, addressId);
            constraintViolations = validator.validate(product);
            while (constraintViolations.size() != 0) {
                numberRejectedOfProducts++;
                name = RandomStringUtils.randomAlphabetic(3, 51);
                product.setName(name);
                constraintViolations = validator.validate(product);
            }
            try {
                products.put(product);
            } catch (InterruptedException e) {
                logger.error("Unable to add new product to queue");
                return;
            }
        }
        logger.info("The number of products rejected by the validator: {}", numberRejectedOfProducts);
    }
}
