package com.pryvarnikov.validators;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Constraint(validatedBy = RestrictionsOnVowelsValidator.class)
public @interface RestrictionsOnVowels {
    String message() default "The field fails the vowel check";

    int minNumberOfVowels() default 3;

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
