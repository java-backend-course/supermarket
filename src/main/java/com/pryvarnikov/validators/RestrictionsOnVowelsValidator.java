package com.pryvarnikov.validators;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;


public class RestrictionsOnVowelsValidator implements ConstraintValidator<RestrictionsOnVowels, String> {
    private int minNumberOfVowels;

    @Override
    public boolean isValid(String field, ConstraintValidatorContext constraintValidatorContext) {
        boolean isContainsVowels = false;
        int currentNumberOfVowels = 0;
        String fieldLowerCase = field.toLowerCase();
        for (int i = 0; i < fieldLowerCase.length(); i++) {
            if (isVowel(fieldLowerCase.charAt(i))) {
                currentNumberOfVowels++;
            }
            if (currentNumberOfVowels == minNumberOfVowels) {
                isContainsVowels = true;
                break;
            }
        }
        return isContainsVowels;
    }

    @Override
    public void initialize(RestrictionsOnVowels constraintAnnotation) {
        minNumberOfVowels = constraintAnnotation.minNumberOfVowels();
    }

    private boolean isVowel(char c) {
        return "aeiou".indexOf(c) != -1;
    }
}
